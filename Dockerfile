FROM node
WORKDIR /app
COPY *.json .
RUN npm install
COPY . .
CMD ["npm", "start"]